package com.alphadev.usshourglassback.worker;

import lombok.Data;

public @Data class Worker {
	private String	name;
	private String	firstName;
	private String	quadry;
	private String	togglApiToken;
}
