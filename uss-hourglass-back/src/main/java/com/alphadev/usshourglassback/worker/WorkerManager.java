package com.alphadev.usshourglassback.worker;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.introspector.BeanAccess;
import org.yaml.snakeyaml.introspector.Property;
import org.yaml.snakeyaml.introspector.PropertyUtils;

import com.alphadev.usshourglassback.toggl.TimeEntry;
import com.alphadev.usshourglassback.toggl.TogglManager;

import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
public class WorkerManager {

	public static String						WORKER_DEF_PATH	= "workers.yml";
	public static String						MAPPING_PATH	= "mapping.yml";

	private Yaml								yaml;
	private List<Worker>						allWorkers;
	private static Map<String, List<String>>	taches;
	private static final SimpleDateFormat		FORMAT_DAY		= new SimpleDateFormat("yyyy-MM-dd");
	private static final SimpleDateFormat		FORMAT_HOUR		= new SimpleDateFormat("HH:mm");

	@Autowired
	private TogglManager						togglManager;

	@PostConstruct
	public void init() throws FileNotFoundException {
		PropertyUtils propertyUtils = new PropertyUtils() {
			@Override
			public Property getProperty(Class<? extends Object> type, String name) {
				if (name.contains("-") || name.contains("_")) {
					name = toCamelCase(name);
				}

				return super.getProperty(type, name);
			}

			private String toCamelCase(String name) {
				String nameFormat = WordUtils.capitalizeFully(name, '_', '-');
				nameFormat = StringUtils.remove(nameFormat, "_");
				nameFormat = StringUtils.remove(nameFormat, '-');
				nameFormat = Character.toLowerCase(nameFormat.charAt(0)) + nameFormat.substring(1);
				return nameFormat;
			}
		};

		Constructor constructor = new Constructor();
		constructor.setPropertyUtils(propertyUtils);
		constructor.getPropertyUtils().setSkipMissingProperties(true);
		constructor.addTypeDescription(new TypeDescription(Worker.class, "!worker"));
		yaml = new Yaml(constructor);
		yaml.setBeanAccess(BeanAccess.FIELD);

		loadWorker();

		taches = yaml.load(new FileInputStream(MAPPING_PATH));

	}

	public List<Worker> loadWorker() throws FileNotFoundException {
		log.info("load all worker");
		allWorkers = new ArrayList<>();
		for (Object w : yaml.loadAll(new FileInputStream(WORKER_DEF_PATH))) {
			Worker w2 = (Worker) w;
			allWorkers.add(w2);
			log.debug(w2);
		}
		return allWorkers;

	}

	@RequestMapping(value = "/api/workers")
	public List<Worker> workers() {
		log.info("call workers");
		return allWorkers;
	}

	@RequestMapping(value = "/api/daily-entries/{quadry}")
	public List<DailyEntry> getDailyEntries(@PathVariable("quadry") String quadry, @RequestParam(name = "date", required = false) String dateParam) {
		List<DailyEntry> dailyEntries = new ArrayList<>();
		log.info("call getDailyEntries: quadry=[" + quadry + "]  date=[" + dateParam + "]");
		LocalDate date = null;
		if (dateParam != null) {
			date = LocalDate.parse(dateParam);
		}

		for (Worker w : allWorkers) {
			if (StringUtils.equals(quadry, w.getQuadry())) {
				List<TimeEntry> tes = togglManager.getDailyEntry(w, date);
				for (TimeEntry te : tes) {
					log.debug("te :: " + te);
					DailyEntry de = new DailyEntry();

					Calendar startDate = Calendar.getInstance();
					startDate.setTime(te.getStart());

					if (te.getStop() != null) {
						Calendar endDate = Calendar.getInstance();
						endDate.setTime(te.getStop());
						de.setEnd(FORMAT_HOUR.format(endDate.getTime()));
					}

					de.setComment(te.getDescription());
					de.setDay(FORMAT_DAY.format(startDate.getTime()));
					de.setStart(FORMAT_HOUR.format(startDate.getTime()));

					de.setDuration(te.getDuration() < 0 ? "en cours" : "" + te.getDuration());
					if (te.getTags() != null && !te.getTags().isEmpty()) {
						de.setProjet(te.getTags().get(0));
					}

					String description = te.getDescription();
					if (description != null) {
						description = description.toUpperCase();
						for (Entry<String, List<String>> task : taches.entrySet()) {
							String tname = task.getKey();
							for (String prefix : task.getValue()) {
								if (description.startsWith(prefix.toUpperCase())) {
									de.setTask(tname);
								}
							}
						}
					}

					if (te.getProjetInfo() != null) {
						de.setTicket(te.getProjetInfo().getKey());
					}

					dailyEntries.add(de);
					log.debug("de :: " + de);
				}
				return dailyEntries;
			}

		}
		return null;
	}
}
