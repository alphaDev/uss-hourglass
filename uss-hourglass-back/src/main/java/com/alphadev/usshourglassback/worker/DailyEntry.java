package com.alphadev.usshourglassback.worker;

import lombok.Data;

public @Data class DailyEntry {
	private String	day;
	private String	start;
	private String	end;
	private String	duration;
	private String	projet;
	private String	task;
	private String	comment;
	private String	ticket;
}
