package com.alphadev.usshourglassback.toggl;

import lombok.Data;

public @Data class UserInfoResponse {

	private long		since;
	private UserInfo	data;

}
