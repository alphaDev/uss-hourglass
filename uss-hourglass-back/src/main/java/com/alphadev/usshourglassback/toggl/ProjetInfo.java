package com.alphadev.usshourglassback.toggl;

import java.util.Date;

import lombok.Data;


public @Data class ProjetInfo {
	private long	totalTime;
	private String	color;
	private String	key;
	private String	pid;
	private Date	lastChange;


}
