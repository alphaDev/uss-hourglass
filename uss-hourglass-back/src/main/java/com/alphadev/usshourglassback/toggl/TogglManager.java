package com.alphadev.usshourglassback.toggl;

import java.net.URL;
import java.text.DateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.alphadev.usshourglassback.worker.Worker;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

import lombok.extern.log4j.Log4j2;

@SuppressWarnings("deprecation")
@Log4j2
@Component
public class TogglManager {
	private static final DateFormat	TOGGL_DATE_FORMATER	= new ISO8601DateFormat();
	public static final long		MILLIS_IN_HOURS		= 1000l * 60l * 60l;

	public List<TimeEntry> getDailyEntry(Worker worker, LocalDate startDateld) {
		Calendar startDate = Calendar.getInstance();
		if (startDateld != null) {
			startDate.clear();
			startDate.set(startDateld.getYear(), startDateld.getMonthValue() - 1, startDateld.getDayOfMonth());
		}
		startDate.add(Calendar.DAY_OF_MONTH, -1);

		Calendar endDate = Calendar.getInstance();
		endDate.clear();
		endDate.set(startDate.get(Calendar.YEAR), startDate.get(Calendar.MONTH), startDate.get(Calendar.DAY_OF_MONTH));
		endDate.add(Calendar.DAY_OF_MONTH, 2);

		String startDateISO8601 = TOGGL_DATE_FORMATER.format(startDate.getTime());
		String endDateISO8601 = TOGGL_DATE_FORMATER.format(endDate.getTime());

		URL url = Network.makeURL("https://www.toggl.com/api/v8/time_entries");
		Map<String, String> arguments = new HashMap<String, String>();
		arguments.put("start_date", startDateISO8601);
		arguments.put("end_date", endDateISO8601);
		List<TimeEntry> weekTimeEntries = new ArrayList<TimeEntry>(Arrays.asList(Network.getAndReadResponse(url, arguments, worker.getTogglApiToken(), "api_token", TimeEntry[].class)));

		Calendar d = Calendar.getInstance();
		d.clear();
		d.set(startDate.get(Calendar.YEAR), startDate.get(Calendar.MONTH), startDate.get(Calendar.DAY_OF_MONTH));
		d.add(Calendar.DAY_OF_MONTH, 1);
		d.add(Calendar.SECOND, 10);
		weekTimeEntries = weekTimeEntries.stream().filter(x -> x.getStart().after(d.getTime())).collect(Collectors.toList());

		Map<String, ProjetInfo> bypid_infoProjets = getProjetInfo(worker);

		for (TimeEntry weekTimeEntry : weekTimeEntries) {
			// log.error("getWeekSummary d [{}]", weekTimeEntry.getStart());
			weekTimeEntry.setProjetInfo(bypid_infoProjets.getOrDefault(weekTimeEntry.getPid(), null));
		}

		return weekTimeEntries;
	}

	public UserInfo getUserInfo(Worker worker) {
		log.debug("Get and store toggl user info");
		URL url = Network.makeURL("https://www.toggl.com/api/v8/me");
		Map<String, String> arguments = new HashMap<String, String>();
		UserInfo userInfo = Network.getAndReadResponse(url, arguments, worker.getTogglApiToken(), "api_token", UserInfoResponse.class).getData();
		return userInfo;
	}

	public Map<String, ProjetInfo> getProjetInfo(Worker worker) {
		log.debug("Get and store toggl projet total time info");

		UserInfo userInfo = getUserInfo(worker);
		URL url = Network.makeURL("https://www.toggl.com/api/v8/workspaces/" + userInfo.getDefault_wid() + "/projects");
		Map<String, String> arguments = new HashMap<String, String>();
		ProjetToggl[] psr = Network.getAndReadResponse(url, arguments, worker.getTogglApiToken(), "api_token", ProjetToggl[].class);

		Map<String, ProjetInfo> bypid_infoProjets = new HashMap<>();
		for (ProjetToggl projet : psr) {
			ProjetInfo pi = new ProjetInfo();
			pi.setKey(projet.getName());
			pi.setPid(projet.getId());
			pi.setColor(projet.getHex_color());
			pi.setTotalTime(projet.getActual_hours() * MILLIS_IN_HOURS);
			pi.setLastChange(projet.getAt());
			bypid_infoProjets.put(pi.getPid(), pi);
		}

		return bypid_infoProjets;

	}
}
