package com.alphadev.usshourglassback.toggl;

import lombok.Data;

public  @Data class UserInfo {
	private String	id;
	private String	api_token;
	private String	default_wid;
	private String	email;
	private String	fullname;
	private String	jquery_timeofday_format;
	private String	jquery_date_format;
	private String	timeofday_format;
	private String	date_format;
	private boolean	store_start_and_stop_time;
	private int		beginning_of_week;
	private String	language;
	private String	image_url;
	private boolean	sidebar_piechart;
	private String	at;
	private int		retention;
	private boolean	record_timeline;
	private boolean	render_timeline;
	private boolean	timeline_enabled;
	private boolean	timeline_experiment;
}
