package com.alphadev.usshourglassback.toggl;

import java.util.Date;
import java.util.List;

import lombok.Data;

public @Data class TimeEntry {
	private String			id;
	private String			wid;
	private String			pid;
	private ProjetInfo		projetInfo;
	private boolean			billable;
	private Date			start;
	private Date			stop;
	private long			duration;
	private String			description;
	private String			created_with;
	private Date			at;
	private List<String>	tags;

	public boolean running() {
		return duration < 0;
	}

	public long calcDuration() {
		if (duration != 0) {
			return duration * 1000;
		}
		if (stop == null && start != null) {
			return System.currentTimeMillis() - start.getTime();
		}
		return duration;
	}

}
