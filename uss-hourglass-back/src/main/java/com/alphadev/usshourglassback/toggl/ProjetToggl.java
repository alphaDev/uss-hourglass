package com.alphadev.usshourglassback.toggl;

import java.util.Date;

import lombok.Data;

public @Data class ProjetToggl {
	private String	id;
	private String	wid;
	private String	cid;
	private String	name;
	private boolean	billable;
	private boolean	is_private;
	private boolean	active;
	private Date	at;
	private long	template_id;
	private String	color;
	private String	hex_color;
	private int		actual_hours;



}
