import { Component, OnInit } from '@angular/core';
import { formatDate } from '@angular/common';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { WorkerService } from './worker.service'
import { Worker } from '../generate-items/items'
import { BehaviorSubject } from "rxjs";

@Component( {
    selector: 'app-list-worker',
    templateUrl: './list-worker.component.html',
    styleUrls: ['./list-worker.component.css']
} )
export class ListWorkerComponent implements OnInit {
    workers: Worker[] = [];
    date: BehaviorSubject<string> = new BehaviorSubject<string>( '' );


    constructor( private workerService: WorkerService ) { }

    ngOnInit() {
        this.workerService.getWorkers().subscribe( workers => this.workers = workers );
    }

    addEvent( type: string, event: MatDatepickerInputEvent<Date> ) {
        this.date.next( formatDate(event.value, 'yyyy-MM-dd', 'en-US') );
        
        console.log( 'date', this.date )
    }
    
    public getDate() {
        return this.date.asObservable();
    }

}
