import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Worker, DailyEntry } from '../generate-items/items';

const httpOptions = {
    headers: new HttpHeaders( { 'Content-Type': 'application/json' } )
};

@Injectable( { providedIn: 'root' } )
export class WorkerService {

    private URL_LIST_WORKERS = '/api/workers';
    private URL_DAILY_ENTRIES = '/api/daily-entries/';

    constructor( private http: HttpClient ) { }

    getWorkers(): Observable<Worker[]> {
        return this.http.get<Worker[]>( this.URL_LIST_WORKERS ).pipe( catchError( this.handleError<Worker[]>( 'list Workers' ) ) );
    }
    getDailyEntries( quadry: string, date: string ): Observable<DailyEntry[]> {
        const options = date ? { params: new HttpParams().set( 'date', date ) } : {};
        return this.http.get<DailyEntry[]>( this.URL_DAILY_ENTRIES + quadry, options ).pipe( catchError( this.handleError<DailyEntry[]>( 'list daily entries for ' + quadry ) ) );
    }

    private handleError<T>( operation = 'operation', result?: T ) {
        return ( error: any ): Observable<T> => {
            console.error( error );
            return of( result as T );
        };
    }
}