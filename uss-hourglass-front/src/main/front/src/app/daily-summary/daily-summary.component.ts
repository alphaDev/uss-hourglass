import { Component, OnInit, Input } from '@angular/core';
import { WorkerService } from '../list-worker/worker.service';
import { Worker, DailyEntry } from '../generate-items/items';
import { Observable } from "rxjs";

@Component( {
    selector: 'app-daily-summary',
    templateUrl: './daily-summary.component.html',
    styleUrls: ['./daily-summary.component.css']
} )
export class DailySummaryComponent implements OnInit {
    @Input() worker: Worker;
    @Input() date: Observable<string>;
    dailyEntries: DailyEntry[] = [];
    displayedColumns: string[] = ['day', 'start', 'end', 'duration', 'projet', 'task', 'comment', 'ticket'];

    constructor( private workerService: WorkerService ) { }

    ngOnInit() {
        if ( this.worker ) {
            this.getDailyEntries( '' );
        }
        this.date.subscribe( date => this.getDailyEntries( date ) );
    }

    getDailyEntries( date ) {
        this.workerService.getDailyEntries( this.worker.quadry, date ).subscribe( dailyEntries => this.dailyEntries = dailyEntries );
    }

}
